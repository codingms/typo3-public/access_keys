# Access-Keys Erweiterung für TYPO3

Diese Extension stellt Tastatur-Kürzel im Backend bereit.


>   #### Achtung: {.alert .alert-danger}
>
>   Wenn ein Benutzer seine Einstellungen ändert, ist es notwendig den kompletten Browser-Tab neu zu laden!


**Features:**

*   Tastatur-Kürzel für: Fokus springt in die Live-Suche
*   Tastatur-Kürzel für: Fokus springt den Speichern-Button in Bearbeitungs-Ansichten
*   Tastatur-Kürzel für: Dashboard wird aufgerufen
*   Tastatur-Kürzel für: Seiten-Cache leeren
*   Tastatur-Kürzel für: Alle Caches leeren

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Access-Keys Dokumentation](https://www.coding.ms/documentation/typo3-access-keys "Access-Keys Dokumentation")
*   [Access-Keys Bug-Tracker](https://gitlab.com/codingms/typo3-public/access_keys/-/issues "Access-Keys Bug-Tracker")
*   [Access-Keys Repository](https://gitlab.com/codingms/typo3-public/access_keys "Access-Keys Repository")
*   [TYPO3 Access-Keys Produktdetails](https://www.coding.ms/typo3-extensions/typo3-access-keys/ "TYPO3 Access-Keys Produktdetails")
