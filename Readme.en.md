# Access-Keys Extension for TYPO3

This extension provides you Keyboard-Shortcut in the Backend.


>   #### Attention: {.alert .alert-danger}
>   
>   If the user changes his settings a complete reload of the browser tab ist required!


**Features:**

*   Keyboard-Shortcut for: Set focus in live-search
*   Keyboard-Shortcut for: Set focus on search button in edit views
*   Keyboard-Shortcut for: Open dashboard module
*   Keyboard-Shortcut for: Clear page cache
*   Keyboard-Shortcut for: Clear all caches

If you need some additional or custom feature - get in contact!


**Links:**

*   [Access-Keys Documentation](https://www.coding.ms/documentation/typo3-access-keys "Access-Keys Documentation")
*   [Access-Keys Bug-Tracker](https://gitlab.com/codingms/typo3-public/access_keys/-/issues "Access-Keys Bug-Tracker")
*   [Access-Keys Repository](https://gitlab.com/codingms/typo3-public/access_keys "Access-Keys Repository")
*   [TYPO3 Access-Keys Produktdetails](https://www.coding.ms/typo3-extensions/typo3-access-keys/ "TYPO3 Access-Keys Produktdetails")
