define(["require", "exports", "jquery"], (function (require, exports, jQuery) {
    "use strict";
    class accessKeysEvents {
        constructor() {
            jQuery(document).keydown(function(event) {
                if(event.ctrlKey) {
                    var shortcut = 'ctrl_' + event.keyCode;
                    if(top.TYPO3.settings.AccessKeys && top.TYPO3.settings.AccessKeys[shortcut]) {
                        accessKeysEvents.keyEvent(top.TYPO3.settings.AccessKeys[shortcut]);
                        event.stopPropagation();
                        return false;
                    }
                }
            });
        }
        static keyEvent(event) {
            if(event === 'focus_live_search') {
                if(window.self !== window.top) {
                    jQuery('#live-search-box', window.top.document).focus();
                }
                else {
                    jQuery('#live-search-box').focus();
                }
            }
            if(event === 'focus_save_button') {
                if(window.self !== window.top) {
                    jQuery('button[name=\'_savedok\']').focus();
                    jQuery('button[name=\'data[save]\']').focus();
                }
                else {
                    jQuery('button[name=\'_savedok\']', top.list_frame.document).focus();
                    jQuery('button[name=\'data[save]\']', top.list_frame.document).focus();
                }
            }
            if(event === 'open_module_dashboard') {
                if(window.self !== window.top) {
                    top.TYPO3.ModuleMenu.App.showModule('dashboard');
                }
                else {
                    TYPO3.ModuleMenu.App.showModule('dashboard');
                }
            }
            if(event === 'clear_cache_pages') {
                if(window.self !== window.top) {
                    window.top.document.getElementsByClassName('dropdown-table-icon-actions-system-cache-clear-impact-low')[0].parentNode.click()
                }
                else {
                    jQuery('.dropdown-table-icon-actions-system-cache-clear-impact-low').closest('a').click();
                }
            }
            if(event === 'clear_cache_all') {
                if(window.self !== window.top) {
                    window.top.document.getElementsByClassName('dropdown-table-icon-actions-system-cache-clear-impact-high')[0].parentNode.click();
                }
                else {
                    jQuery('.dropdown-table-icon-actions-system-cache-clear-impact-high').closest('a').click();
                }
            }
        }
    }
    return new accessKeysEvents
}));
