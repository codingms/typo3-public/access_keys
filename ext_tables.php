<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-preProcess'][]
    = \CodingMs\AccessKeys\Hook\PageRendererHook::class . '->addJSCSS';

//
// Add new fields to user settings
$addFields = [
    'focus_live_search',
    'focus_save_button',
    'open_module_dashboard',
    'clear_cache_pages',
    'clear_cache_all',
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToUserSettings('--div--;Keys');
foreach ($addFields as $addField) {
    $GLOBALS['TYPO3_USER_SETTINGS']['columns'][$addField] = [
        'label' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.' . $addField,
        'type' => 'select',
        'items' => [
            '' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.no_key',
            'ctrl_65' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_65',
            'ctrl_66' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_66',
            'ctrl_67' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_67',
            'ctrl_68' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_68',
            'ctrl_69' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_69',
            'ctrl_70' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_70',
            'ctrl_71' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_71',
            'ctrl_72' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_72',
            'ctrl_73' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_73',
            'ctrl_74' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_74',
            'ctrl_75' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_75',
            'ctrl_76' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_76',
            'ctrl_77' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_77',
            'ctrl_78' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_78',
            'ctrl_79' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_79',
            'ctrl_80' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_80',
            'ctrl_81' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_81',
            'ctrl_82' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_82',
            'ctrl_83' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_83',
            'ctrl_84' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_84',
            'ctrl_85' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_85',
            'ctrl_86' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_86',
            'ctrl_87' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_87',
            'ctrl_88' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_88',
            'ctrl_89' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_89',
            'ctrl_90' => 'LLL:EXT:access_keys/Resources/Private/Language/locallang.xlf:tx_accesskeys.ctrl_90',
        ],
    ];
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToUserSettings($addField);
}
