# Access-Keys Change-Log

## 2023-09-08  Release of version 1.0.7

*	[TASK] Optimize documentation



## 2023-01-13  Release of version 1.0.6

*	[TASK] Integrate documentation files
*	[BUGFIX] Fix page renderer hook issue



## 2022-05-15  Release of version 1.0.5

*	[BUGFIX] Fix JavaScript issue when no shortcuts are configured



## 2022-05-15  Release of version 1.0.4

*	[BUGFIX] Fix extension kex in composer.json



## 2022-05-15  Release of version 1.0.3

*	[BUGFIX] Add missing extension kex in composer.json



## 2022-05-15  Release of version 1.0.2

*	[BUGFIX] Prevent access on undefined array index



## 2022-01-13  Release of version 1.0.1

*	[BUGFIX] Fix extension configuration
*	[TASK] Add extension icon



## 2022-01-13  Release of version 1.0.0

*	[TASK] Initial release

