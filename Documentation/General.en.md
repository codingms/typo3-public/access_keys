# Access-Keys for TYPO3

This extension provides an Access-Keys for TYPO3 backend. This Shortcuts are configurable for each user in his user-settings. Initially the shortcuts are not configured.
