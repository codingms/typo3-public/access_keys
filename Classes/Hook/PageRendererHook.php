<?php declare(strict_types=1);

namespace CodingMs\AccessKeys\Hook;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2021 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AccessKeys\Utility\AuthorizationUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Page\PageRenderer;

/**
 * Add required JavaScript events
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class PageRendererHook
{

    /**
     * @param array $parameters An array of available parameters
     * @return void
     */
    public function addJSCSS($parameters)
    {
        /** @var PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        if ($pageRenderer->getApplicationType() === 'BE') {
            $pageRenderer->loadRequireJsModule('TYPO3/CMS/AccessKeys/Backend/Events');
            $user = $this->getBackendUserAuthentication();
            $fields = [
                'focus_live_search',
                'focus_save_button',
                'open_module_dashboard',
                'clear_cache_fe',
                'clear_cache_all',
            ];
            foreach ($fields as $field) {
                if (isset($user->uc[$field])) {
                    $pageRenderer->addInlineSetting('AccessKeys', $user->uc[$field], $field);
                }
            }
        }
    }

    /**
     * @return BackendUserAuthentication|null
     */
    protected function getBackendUserAuthentication()
    {
        return $GLOBALS['BE_USER'] ?? null;
    }

}
